package utils;

import java.text.NumberFormat;

public class Utils {
	public static double mean(double... nums) {
		double tot = 0;
		for (int i = 0; i < nums.length; i++)
			tot += nums[i];
		return nums.length > 0 ? tot / nums.length : Double.NaN;
	}

	public static double stdev(double... nums) {
		double var = 0;
		double avg = mean(nums);

		for (int i = 0; i < nums.length; i++)
			var += (nums[i] - avg) * (nums[i] - avg);

		var /= nums.length;
		return Math.sqrt(var);
	}

	public static String stats(double[] data, boolean printdev, NumberFormat df) {
		return df.format(mean(data)) + (printdev ? "\t" + df.format(stdev(data)) : "");
	}

}
