package neuralfield;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class ResponseField extends JTextField implements
		KeyListener {
	private static final long serialVersionUID = 1710272783041115665L;
	public String TEXT;
	public double VALUE;

	public ResponseField(String name, String init) {
		super(init);

		setLayout(new BorderLayout(0, 10));

		JLabel label = new JLabel(name);
		label.setForeground(Color.LIGHT_GRAY);
		label.setBorder(new EmptyBorder(1, 1, 1, 5));
		add(label, BorderLayout.EAST);
		setPreferredSize(new Dimension(150, 25));
		setMaximumSize(new Dimension(2000, 25));

		TEXT = init;
		addKeyListener(this);
		updateColors();
	}

	@Override
	protected void fireActionPerformed() {
		this.TEXT = getText();
		try {
			this.VALUE = Double.parseDouble(TEXT);
		} catch (NumberFormatException ne) {
			this.VALUE = Double.NaN;
		}
		updateColors();

		super.fireActionPerformed();
	}
	public void keyPressed(KeyEvent evt) {
		updateColors();
	}

	public void keyTyped(KeyEvent evt) {
		updateColors();
	}

	public void keyReleased(KeyEvent evt) {
		updateColors();
	}

	private void updateColors() {
		if (TEXT.equals(getText())) {
			if (Double.isNaN(VALUE)) {
				setBackground(new Color(250, 200, 200));
				setBorder(new LineBorder(new Color(100, 0, 0)));
			} else {
				setBackground(new Color(200, 250, 200));
				setBorder(new LineBorder(new Color(0, 100, 0)));
			}
		} else {
			setBackground(new Color(250, 250, 200));
			setBorder(new LineBorder(new Color(100, 100, 0)));
		}
	}
}
