package neuralfield;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import utils.Methods;

public class NeuralSim1D extends JPanel implements Runnable {
	private static final long serialVersionUID = -8817746756390773072L;

	// Graphics Variables
	private Rectangle wrect = new Rectangle(0, 0, 1, 1), urect = new Rectangle(
			0, 0, 1, 1);
	public SwingPanel controls;
	int mouseDown = 0;

	// Simulation Variables
	public final int N;
	private double[] u;
	private double[] p;
	private double v;

	double[][] W;
	private double[][] ww;

	private double[] I;
	private double[] Z;

	// delay variables
	private double[] u_pastD;
	private double[][] w_pastDp;

	// other variables
	public double slowFactor = 10000;

	public boolean paused = true;

	public NeuralSim1D(int n) {

		this.N = n;
		u = new double[n];
		p = new double[n];
		I = new double[n];
		Z = new double[n];

		v = 0;
		W = new double[n][n];
		ww = new double[n][n];

		u_pastD = new double[n];
		w_pastDp = new double[n][n];

		for (int i = 0; i < n; i++) {
			Z[i] = 0.3;
			p[i] = 1;
		}

		EqnModel.L = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				// if (i == j)
				// W[i][j] = 1;
				if (i == (j + 1) % n)
					W[i][j] = 0.35;
				// else if (i == (j + 2) % n)
				// W[i][j] = -0.2;
				else if ((i + 1) % n == j)
					W[i][j] = -0.35;
				// else if ((i + 2) % n == j)
				// W[i][j] = -0.2;

				// else
				// W[i][j] = (Math.random() - 0.5) / 2;

				// W[i][j] =
				// gaussian((i - j) * (i - j))
				// + Math.sin((i - j) / 15D)*0.5;
			}
		}

		addMouseListener(new MouseHandler());
		setBackground(Color.white);
		controls = new SwingPanel(this);
	}

	private class MouseHandler extends MouseAdapter {
		public void mousePressed(MouseEvent evt) {
			mouseDown = evt.isMetaDown() ? -1 : 1;
		}

		public void mouseReleased(MouseEvent evt) {
			mouseDown = 0;
		}
	}

	public int toScreen(int ind) {
		return urect.x + (ind * urect.width) / N;
	}

	public int fromScreen(int x) {
		return (N * (x - urect.x)) / urect.width;
	}

	private void updateRects() {
		urect = new Rectangle(50, 25, getWidth() - 100, 200);
		wrect = new Rectangle(150, 275, 400, 400);
	}

	public void run() {
		long lastTime = System.currentTimeMillis();

		double time = 0;

		while (true) {
			if (paused) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {}
				continue;
			}

			updateRects();

			long currTime = System.currentTimeMillis();

			double dt = (currTime - lastTime) / 1000D; // real time
			dt /= slowFactor;

			Point locos = getLocationOnScreen();
			Point mouse = MouseInfo.getPointerInfo().getLocation();
			mouse.translate(-locos.x, -locos.y);

			// mouse input
			if (urect.contains(mouse)) {
				int index = fromScreen(mouse.x);

				for (int j = 0; j < N; j++) {
					if (j == index)
						I[j] = 100 * mouseDown;
					else
						I[j] = 0;
				}
			} else if (wrect.contains(mouse)) {
				int x = (N * (mouse.x - wrect.x)) / wrect.width, y =
						(N * (mouse.y - wrect.y)) / wrect.height;

				for (int i = Math.max(0, x - 3); i < Math.min(N, x + 3); i++)
					for (int j = Math.max(0, y - 3); j < Math.min(N, y + 3); j++)
						W[i][j] +=
								mouseDown
										* 0.001
										* gaussian((x - i) * (x - i) + (y - j)
												* (y - j));
			}

			// prepare delays

			// simulate now!
			for (int j = 0; j < N; j++) {
				u[j] += dt * EqnModel.duj_dt(j, time, u, p, W, I[j], v);
				u[j] = Methods.bound(u[j], 0, 1);

				p[j] += dt * EqnModel.dpj_dt(j, u, p);
				v += dt * EqnModel.dv_dt(v, u, Z);

				if (controls.train.isSelected())
					for (int k = 0; k < N; k++) {
						ww[j][k] += dt * EqnModel.dwjk_dt(j, k, u, u_pastD, ww);
						W[j][k] += dt * EqnModel.dWjk_dt(j, k, W, w_pastDp);
					}
			}

			repaint();

		}
	}

	static double gaussian(double v) {
		return Math.exp(-(v) / 2);
	}

	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);
		Graphics2D g = (Graphics2D) gg;

		// u and p variables
		for (int i = 0; i < N; i++) {
			int x = toScreen(i);

			int mh = (int) urect.getMaxY(), h = urect.height / 2;

			g.setColor(Color.LIGHT_GRAY);
			g.fillOval(x - 2, (int) (mh - p[i] * h) - 5, 5, 5);
			g.drawLine(x, (int) (mh - p[i] * h), x, mh);

			g.setColor(Color.black);
			g.fillOval(x - 5, (int) (mh - u[i] * h) - 5, 10, 10);
			g.drawLine(x - 1, (int) (mh - u[i] * h), x - 1, mh);

		}

		// inhibitory neuron
		g.setColor(Color.RED);
		g.fillOval(urect.x + (int) (v * 100), urect.y, 10, 10);
		g.drawLine(urect.x, urect.y + 5, urect.x + (int) (v * 100), urect.y + 5);

		// weight drawing
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (W[i][j] < 0)
					g.setColor(
							Methods.colorMeld(Color.white, Color.blue,
									Methods.bound(-W[i][j], 0, 1)));
				else
					g.setColor(
							Methods.colorMeld(Color.WHITE, Color.RED,
									Methods.bound(W[i][j], 0, 1)));

				g.fillRect(wrect.x + (i * wrect.width) / N, wrect.y
						+ (j * wrect.height) / N, wrect.width / N + 1,
						wrect.height / N + 1);
			}
		}
		g.setColor(Color.BLACK);
		g.draw(wrect);
	}
	public static void main(String[] args) {
		NeuralSim1D panel = new NeuralSim1D(100);

		JFrame frame = new JFrame("Neural Field Simulation 1D");
		Methods.centerFrame(1200, 800, frame);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel, java.awt.BorderLayout.CENTER);
		frame.add(panel.controls, java.awt.BorderLayout.EAST);
		frame.setVisible(true);
		
		panel.paused = false;

		new Thread(panel).start();
	}
}
