package neuralfield;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.border.EmptyBorder;

import utils.Methods;

public class SwingPanel extends Box implements ActionListener {
	private static final long serialVersionUID = -573481842752125478L;

	JCheckBox train = new JCheckBox("Train Dynamically");
	ResponseField speedBox = new ResponseField("Speed", "10000");
	JButton smooth = new JButton("Smooth");
	JButton transp = new JButton("Make Antisymmetric");
	JButton reset1 = new JButton("Reset");
	JButton reset2 = new JButton("Preset 2");
	JButton reset3 = new JButton("Preset 3");

	NeuralSim1D sim;

	public SwingPanel(NeuralSim1D owner) {
		super(BoxLayout.Y_AXIS);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.sim = owner;

		setOpaque(true);

		train.setFocusable(false);
		smooth.setFocusable(false);
		transp.setFocusable(false);

		speedBox.addActionListener(this);
		smooth.addActionListener(this);
		transp.addActionListener(this);
		reset1.addActionListener(this);
		reset2.addActionListener(this);
		reset3.addActionListener(this);

		add(train);
		add(speedBox);
		add(Box.createGlue());
		add(smooth);
		add(transp);
		add(reset1);
		add(reset2);
		add(reset3);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == speedBox && !Double.isNaN(speedBox.VALUE)) {
			System.out.println(speedBox.VALUE);
			sim.slowFactor = speedBox.VALUE;
		} else if (evt.getSource() == smooth) {
			int smooth = 3;

			double[][] newW = new double[sim.N][sim.N];
			for (int i = 0; i < sim.N; i++)
				for (int j = 0; j < sim.N; j++)
					for (int k = -smooth / 2; k <= smooth / 2; k++)
						for (int l = -smooth / 2; l <= smooth / 2; l++) {
							int x = (int) Methods.bound(i + k, 0, sim.N - 1), y =
									(int) Methods.bound(j + l, 0, sim.N - 1);
							newW[i][j] += sim.W[x][y] / (smooth * smooth);
						}

			sim.W = newW;
		} else if (evt.getSource() == transp) {
			for (int i = 0; i < sim.N; i++)
				for (int j = i; j < sim.N; j++) {
					double temp = (sim.W[i][j] - sim.W[j][i]) / 2;
					sim.W[i][j] = temp;
					sim.W[j][i] = -temp;
				}
		} else if (evt.getSource() == reset1) {
			for (int i = 0; i < sim.N; i++) {
				for (int j = 0; j < sim.N; j++) {
					if (i == (j + 1) % sim.N)
						sim.W[i][j] = 0.35;
					else if ((i + 1) % sim.N == j)
						sim.W[i][j] = -0.35;
					else
						sim.W[i][j] = 0;
				}
			}
		} else if (evt.getSource() == reset2) {
			for (int i = 0; i < sim.N; i++) {
				for (int j = 0; j < sim.N; j++) {
					if (i == (j + 1) % sim.N)
						sim.W[i][j] = 0.35;
					else if (i == (j + 2) % sim.N)
						sim.W[i][j] = -0.2;
					else if ((i + 1) % sim.N == j)
						sim.W[i][j] = 0.35;
					else if ((i + 2) % sim.N == j)
						sim.W[i][j] = -0.2;
					else
						sim.W[i][j] = 0;

				}
			}

		} else if (evt.getSource() == reset3) {
			for (int i = 0; i < sim.N; i++) {
				for (int j = 0; j < sim.N; j++) {
					sim.W[i][j] =
							NeuralSim1D.gaussian((i - j) * (i - j))
									+ Math.sin((i - j) / 15D) * 0.5;
				}
			}
		}
	}
}
