package neuralfield;

public class EqnModel {
	// NORMAL EQN CONSTANTS:
	public static double tau = 0.01;
	public static double tau_f = 1;
	public static double theta = 0.5;
	public static double theta_v = 0.5;
	public static double L = 0.6;
	public static double pmax = 2;

	// DYNAMIC LAG EQN VARIABLES
	public static double tau_w = 150;
	public static double tau_I = tau_w;
	public static double M = 1;
	public static double gamma_p = 3614.5;
	public static double gamma_d = 150;
	public static double w_max = 0.4852;
	public static double I_s = 20;
	public static double I_h = 20;

	public static double D = .03;
	public static double D_p = .02;

	public static double duj_dt(int j, double t, double[] U, double[] P,
			double[][] W,
			double I, double v) {
		double sum = 0;

		for (int k = 0; k < U.length; k++)
			if (k != j)
				sum += W[j][k] * P[k] * U[k];

		return (-U[j] + activation(I + U[j] + sum - L * v - theta))
				/ tau;

		// old model, with special [jj] case.
		// return (-U[j] + activation(I + W[j][j] * U[j] + sum - L * v - theta))
		// / tau;
	}

	public static double dpj_dt(int j, double[] U, double[] P) {
		return (1 - P[j] + (pmax - 1) * U[j]) / tau_f;
	}

	public static double dv_dt(double v, double[] U, double[] Z) {
		double sum = 0;
		for (int k = 1; k < U.length; k++)
			sum = sum + Z[k] * U[k];

		return (-v + activation(sum - theta_v)) / tau;
	}

	public static double dwjk_dt(int j, int k, double[] U, double[] U_pastD,
			double[][] w) {

		return (-gamma_d * w[j][k] * U_pastD[k] * (M - U[j]) +
				gamma_p * (w_max - w[j][k]) * U_pastD[k] * U[j]) / tau_w;
	}

	public static double
			dWjk_dt(int j, int k, double[][] W, double[][] w_pastDp) {
		return (w_pastDp[j][k] - W[j][k]) / tau_I;
	}

	public static double activation(double xi) {
		return xi > 0 ? 1 : 0; // heaviside function
		// return 1/(1 + Math.exp(-100*xi)); // smoother activation function
	}

}
