package neuralfield;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Presentation extends JPanel implements KeyListener {
	private static final long serialVersionUID = 999608020028948450L;
	public static Dimension SCREEN = Toolkit.getDefaultToolkit()
			.getScreenSize();

	public static final int NUM_SLIDES = 19, SIM_INDEX = 15;
	public static ImageIcon[] IMAGES = new ImageIcon[NUM_SLIDES];

	JLabel display = new JLabel();
	NeuralSim1D sim = new NeuralSim1D(100);

	int number = 0;
	static {
		for (int i = 0; i < NUM_SLIDES; i++) {
			Image img =
					new ImageIcon("Resources\\present\\Slide" + (i+1) + ".png").getImage();
			IMAGES[i] =
					new ImageIcon(img.getScaledInstance(img.getWidth(null)
							* SCREEN.height / img.getHeight(null),
							SCREEN.height, Image.SCALE_SMOOTH));
		}
	}

	public Presentation() {
		setLayout(new BorderLayout());
		setBackground(Color.BLACK);
		add(display, BorderLayout.CENTER);
		addKeyListener(this);
		display.addKeyListener(this);

		display.setIcon(IMAGES[0]);
		display.setHorizontalAlignment(JLabel.CENTER);
		
		sim.controls.setBackground(new Color(238,238,238));
		sim.controls.train.addKeyListener(this);
		sim.controls.speedBox.addKeyListener(this);
		sim.controls.transp.addKeyListener(this);
		
		new Thread(sim).start();
	}

	@Override
	public void keyReleased(KeyEvent evt) {

		if (evt.getKeyCode() == KeyEvent.VK_SPACE
				|| evt.getKeyCode() == KeyEvent.VK_RIGHT)
			update((number + 1) % NUM_SLIDES);
		else if (evt.getKeyCode() == KeyEvent.VK_LEFT)
			update((number + NUM_SLIDES - 1) % NUM_SLIDES);
		else if (evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57)
			update(evt.getKeyCode() - 48);
	}

	public void update(int newNum) {
		newNum = newNum % NUM_SLIDES;
		if (newNum == SIM_INDEX) {
			removeAll();
			add(sim, BorderLayout.CENTER);
			add(sim.controls, BorderLayout.EAST);
			invalidate();
			revalidate();
			repaint();
			sim.paused = false;
		}  else {
			sim.paused = true;
			display.setIcon(IMAGES[newNum]);
			removeAll();
			add(display, BorderLayout.CENTER);
			invalidate();
			revalidate();
			repaint();
		}
		number = newNum;
	}

	public void keyPressed(KeyEvent evt) {}

	public void keyTyped(KeyEvent evt) {}

	public static void main(String[] args) {
		JFrame frame = new JFrame("REU Presentation");
		// Visualization v = new Visualization();
		Presentation p = new Presentation();
		frame.add(p, BorderLayout.CENTER);
		frame.addKeyListener(p);
		// frame.add(v.controls, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setSize(SCREEN);
		frame.setVisible(true);
	}

}
