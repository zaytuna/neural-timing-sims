package tubules;

public interface UpdateCallback {
	public void onUpdate(double f);
}
