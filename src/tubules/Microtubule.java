package tubules;

import java.util.concurrent.LinkedBlockingDeque;

import math.Vector3D;

public class Microtubule {
	LinkedBlockingDeque<Tubulin> savedPoints =
			new LinkedBlockingDeque<Tubulin>();
	Vector3D pos, velo;
	double freedom = 1;

	public Microtubule(Vector3D pos, Vector3D velo) {
		this.pos = pos;
		this.velo = velo;
	}

	public class Tubulin extends Vector3D {
		public Tubulin(Vector3D clone) {
			this.x = clone.x;
			this.y = clone.y;
			this.z = clone.z;
		}

		double GTP2GDP; // out of 1;
	}
}
