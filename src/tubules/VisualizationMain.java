package tubules;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JPanel;

import math.Eye;
import math.Vector3D;
import utils.Methods;

public class VisualizationMain extends JPanel implements UpdateCallback,
		KeyListener {
	private static final long serialVersionUID = -3692407444992661769L;

	public HashSet<Integer> keys = new HashSet<Integer>();

	Color TUBE_COLOR = Color.CYAN;
	Color PART_COLOR = Color.WHITE;

	Simulation sim;
	Eye eye;

	public VisualizationMain(Dimension size) {
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);

		setBackground(Color.black);
		sim = new Simulation(10);
		sim.init(new Simulation.Params(2, 5, 10));
		sim.addUpdateCallback(this);

		eye = new Eye(size);

		addKeyListener(this);

		new Thread(sim).start();
	}

	public void paintComponent(Graphics g1) {
		super.paintComponent(g1);

		Graphics2D g = (Graphics2D) g1;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		eye.synchronize();

		g.setColor(TUBE_COLOR);
		g.setStroke(new BasicStroke(2));

		for (Microtubule t : sim.tubes) {
			Vector3D last = null;

			for (Vector3D save : t.savedPoints) {
				Vector3D scr = eye.toScreen(save);
				if (scr != null && last != null)
					g.drawLine(eye.psX(scr.x), eye.psY(scr.y), eye.psX(last.x),
							eye.psY(last.y));
				last = scr;
			}
		}

		g.setColor(PART_COLOR);

		for (Particle par : sim.part) {
			Vector3D scr = eye.toScreen(par.pos);
			if (scr != null)
				g.fillRect(eye.psX(scr.x), eye.psY(scr.y), 10, 10);
		}

		g.setColor(Color.magenta);
		g.drawString("" + sim.fps, 10, 20);
	}

	public void onUpdate(double factor) {
		double diff = 0.0009;

		if (keys.contains(KeyEvent.VK_E))
			eye.da += diff / factor;
		if (keys.contains(KeyEvent.VK_Q))
			eye.da -= diff / factor;
		if (keys.contains(KeyEvent.VK_X))
			eye.db += diff / factor;
		if (keys.contains(KeyEvent.VK_Z))
			eye.db -= diff / factor;

		eye.update(factor);
		eye.updatePosition();
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		keys.add(evt.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent evt) {
		keys.remove(evt.getKeyCode());
	}

	@Override
	public void keyTyped(KeyEvent a) {}

	public static void main(String[] args) {
		JFrame frame = new JFrame("REU Simulations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		VisualizationMain sim = new VisualizationMain(new Dimension(1200, 800));
		frame.add(sim);
		frame.addKeyListener(sim);
		frame.pack();

		Methods.centerFrame(frame.getWidth(), frame.getHeight(), frame);

		frame.setVisible(true);
	}

}
