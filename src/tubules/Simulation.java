package tubules;

import java.util.ArrayList;

import math.Vector2D;
import math.Vector3D;

public class Simulation implements Runnable {
	public static class Params {
		final int DIMENSION;
		final int NUM_START;
		final int NUM_PARTICLES;

		public Params(int dim, int start, int part) {
			this.DIMENSION = dim;
			this.NUM_START = start;
			this.NUM_PARTICLES = part;
		}
	}

	ArrayList<Microtubule> tubes = new ArrayList<Microtubule>();
	ArrayList<Particle> part = new ArrayList<Particle>();

	private final int delay;
	public int fps;
	Params params;

	ArrayList<UpdateCallback> listeners = new ArrayList<UpdateCallback>();

	// TODO: keep track of something for simulations

	/**
	 * Constructs a simulation object
	 * 
	 * @param speed
	 *        , in ms to sleep. A value of '-1' indicates fastest possible simulation
	 */
	public Simulation(int speed) {
		this.delay = speed;

	}

	/**
	 * Constructs simulation according to parameters
	 */
	public void init(Params p) {
		this.params = p;

		switch (p.DIMENSION) {
		case 2:
			for (int i = 0; i < params.NUM_START; i++)
				tubes.add(new Microtubule(new Vector3D(), Vector2D.randomUnit()
						.to3DV()));

			for (int i = 0; i < params.NUM_PARTICLES; i++)
				part.add(new Particle(Vector2D.randomUnit().to3DV()));

			break;
		case 3:
			for (int i = 0; i < params.NUM_START; i++)
				tubes.add(new Microtubule(new Vector3D(), Vector3D.random(1.0)));

			for (int i = 0; i < params.NUM_PARTICLES; i++)
				part.add(new Particle(Vector3D.random(1.0)));
			break;
		}

	}

	/**
	 * This is the overriden run method for Runnable; this actually provides a simulation run.
	 */
	public void run() {
		long lastTime = System.currentTimeMillis();

		while (true) {
			long currentTime = System.currentTimeMillis();
			int mstime = (int) (currentTime - lastTime);
			lastTime = currentTime;

			if (delay > 0) {
				try {
					Thread.sleep(delay);
				} catch (Exception e) {}

				double factor = mstime / 10D; // factor away from 100fps
				this.fps = (int) (1000D / mstime);

				for (UpdateCallback cb : listeners)
					cb.onUpdate(factor);
			}

			for (Microtubule m : tubes) {
				m.freedom += (Math.random() - 0.5) * (mstime / 30d);
				if (m.freedom > 0) {
					m.savedPoints.add(m.new Tubulin(m.pos.clone()));
					m.pos.add(m.velo.clone().scale(mstime / 1000d));
				} else {
					if (!m.savedPoints.isEmpty())
						m.pos = m.savedPoints.pollLast();
				}
			}

			for (Particle p : part) {
				p.pos.add(Vector3D.random(mstime / 1000d));
			}
		}
	}

	public void addUpdateCallback(UpdateCallback cb) {
		listeners.add(cb);
	}
}
